#!/bin/bash

########################################################################
#                                                                      #
#   This script voidifies your xfce theme on Void Linux; of cause      #
#   you can use it on other systems.                                   #
#                                                                      #
#   The gtk theme, the xfwm4 theme, and the icon theme are not mine,   #
#   I just did some changes to the original themes:                    #
#   - Skeuos-Green-Dark                                                #
#   - kora-green                                                       #
#   The launcher icon and the wallpaper are created by me, though.     #
#                                                                      #
########################################################################

## theme names
          theme="voidify-Skeuos-Green-Dark"
          icons="voidify-kora-green"
  launcher_icon="void-logo-bw-80pc.svg"
      wallpaper="void_3d_4k.png"
       docklike="$HOME/.config/xfce4/panel/docklike-15.rc"

## create stuff if necessary
  [ -f "$docklike" ] && cp "$docklike" "$docklike".backup
  [ ! -d "$HOME/.local/share/wallpapers/" ] && mkdir -p $HOME/.local/share/wallpapers/

## remove old version of this theme if necessary; always start fresh ...take a shower
  [ -d "$HOME/.themes/$theme" ]                     && rm -rf $HOME/.themes/"$theme"
  [ -d "$HOME/.icons/$icons" ]                      && rm -rf $HOME/.icons/"$icons"
  [ -f "$HOME/.icons/$launcher_icon" ]              && rm -f  $HOME/.icons/"$launcher_icon"
  [ -f "$HOME/.local/share/wallpapers/$wallpaper" ] && rm -f  $HOME/.local/share/wallpapers/"$wallpaper"

## copy theme files
  cp -R ./"$theme"/        $HOME/.themes/
  cp -R ./"$icons"/        $HOME/.icons/
  cp    ./"$launcher_icon" $HOME/.icons/
  cp    ./"$wallpaper"     $HOME/.local/share/wallpapers/

## apply theme; could be so much easier if xfce themeing would be more centralized
  # gtk theme
  xfconf-query -c xsettings \
               -p /Net/ThemeName \
               -s $theme \
  > /dev/null 2>&1; [ "$?" -eq 1 ] && \
  xfconf-query -c xsettings \
               -p /Net/ThemeName \
               -n -t string \
               -s $theme
  # icon theme
  xfconf-query -c xsettings \
               -p /Net/IconThemeName \
               -s $icons \
  > /dev/null 2>&1; [ "$?" -eq 1 ] && \
  xfconf-query -c xsettings \
               -p /Net/IconThemeName \
               -n -t string \
               -s $icons
  # xfwm4 theme
  xfconf-query -c xfwm4 \
               -p /general/theme \
               -s $theme \
  > /dev/null 2>&1; [ "$?" -eq 1 ] && \
  xfconf-query -c xfwm4 \
               -p /general/theme \
               -n -t string \
               -s $theme
  # wallpaper
  screen="$(xfconf-query -c xfce4-desktop -l | head -n 1)"
  xfconf-query -c xfce4-desktop \
               -p ${screen%/*}/last-image \
               -s $HOME/.local/share/wallpapers/"$wallpaper"
  # panel dark mode
  xfconf-query -c xfce4-panel \
               -p /panels/dark-mode \
               -s true

  xfconf-query -c xfce4-panel \
               -p /panel-$i/icon-size \
               -s 0
## docklike.rc; no worries, your original settings are backed up, I promissed
## there: $HOME/.config/xfce4/panel/docklike-15.rc.backup; just remove the ".backup" bit
  if [ -f "$docklike" ]; then
    sed -i "s/^indicatorStyle.*/indicatorStyle=0/" "$docklike"
    sed -i "s/^indicatorOrientation=.*/indicatorOrientation=1/" "$docklike"
    sed -i "s/^indicatorColor.*/indicatorColor=rgb(9,191,156)/" "$docklike"
    sed -i "s/^inactiveColor.*/inactiveColor=rgb(3,86,70)/" "$docklike"
  else
    cat > "$docklike" << EOF
[user]
indicatorStyle=0
indicatorOrientation=1
indicatorColor=rgb(9,191,156)
inactiveColor=rgb(3,86,70)
EOF
  fi
## launcher icon; frustratingly complex affair for such a silly change
## hate it... works, though
  # application menu
  appmenu=$(xfconf-query -c xfce4-panel -lv | grep applicationsmenu)
  if [ -n "$appmenu" ]; then
      xfconf-query -c xfce4-panel \
                   -p ${appmenu%% *}/button-icon \
                   -s $HOME/.icons/$launcher_icon \
      > /dev/null 2>&1; [ "$?" -eq 1 ] && \
      xfconf-query -c xfce4-panel \
                   -p ${appmenu%% *}/button-icon \
                   -n -t string \
                   -s $HOME/.icons/$launcher_icon
      xfconf-query -c xfce4-panel \
                   -p ${appmenu%% *}/show-button-title \
                   -s false \
      > /dev/null 2>&1; [ "$?" -eq 1 ] && \
      xfconf-query -c xfce4-panel \
                   -p ${appmenu%% *}/show-button-title \
                   -n -t bool \
                   -s false
  fi
  # whisker menu
  whisker=$(xfconf-query -c xfce4-panel -lv | grep whisker)
  if [ -n "$whisker" ]; then
      xfconf-query -c xfce4-panel \
                   -p ${whisker%% *}/button-icon \
                   -s $HOME/.icons/$launcher_icon \
      > /dev/null 2>&1; [ "$?" -eq 1 ] && \
      xfconf-query -c xfce4-panel \
                   -p ${whisker%% *}/button-icon \
                   -n -t string \
                   -s $HOME/.icons/$launcher_icon
      xfconf-query -c xfce4-panel \
                   -p ${whisker%% *}button-title \
                   -s false \
      > /dev/null 2>&1; [ "$?" -eq 1 ] && \
      xfconf-query -c xfce4-panel \
                   -p ${whisker%% *}/button-title \
                   -n -t bool \
                   -s false
  fi
# set panel icon size to auto; maybe a one-liner should do the trick? Of cause not.
  num_of_panels=$(xfconf-query -c xfce4-panel -l \
                            | grep "/panels/panel-" \
                            | sort -V \
                            | tail -n 1) \
                            | sed 's/[^0-9]*//g'
  for i in $(seq 1 $num_of_panels); do
    xfconf-query -c xfce4-panel \
                 -p /panels/panel-$i/icon-size \
                 -s 0 \
    > /dev/null 2>&1; [ "$?" -eq 1 ] && \
    xfconf-query -c xfce4-panel \
                 -p /panels/panel-$i/icon-size \
                 -n -t int \
                 -s 0
  done

## reload panel ...finally
  xfce4-panel -r
