#!/bin/bash

## theme names
  icontheme="kora-green"
  gtktheme="Skeuos-Green-Dark"
  xfwmtheme="Skeuos-Green-Dark-XFWM"
  wallpaper="void_3d_4k.png"
  launchericon="void_mouse.svg"

## remove old version of this theme if necessary
  [ -e "/usr/share/icons/$icontheme" ] && sudo rm -rf "/usr/share/icons/$icontheme"
  [ -e "/usr/share/themes/$gtktheme" ] && sudo rm -rf "/usr/share/themes/$gtktheme"
  [ -e "/usr/share/themes/$xfwmtheme" ] && sudo rm -rf "/usr/share/themes/$xfwmtheme"
  [ -e "/usr/share/icons/$launchericon" ] && sudo rm -f "/usr/share/icons/$launchericon"
  [ -e "/usr/share/backgrounds/xfce/$wallpaper" ] && sudo rm -f "/usr/share/backgrounds/xfce/$wallpaper"

## apply theme
  xfconf-query -c xfwm4 -p /general/theme -s default
  xfconf-query -c xsettings -p /Net/ThemeName -s default
  xfconf-query -c xsettings -p /Net/IconThemeName -s default
  screen=$(xfconf-query -c xfce4-desktop -l | head -n 1)
  xfconf-query -c xfce4-desktop -p "${screen%/*}/last-image" -s /usr/share/backgrounds/xfce/xfce-blue.jpg
  xfconf-query -c xfce4-panel -p /panels/dark-mode -s false
  xfce4-panel -r
